# README #
Technical test developer.
## Prerequisites ##
* Git pull repository.
* Install Java 11.
* Open the project with IDE what support Gradle build tool.
## Run the application ##
* Find the class AvBankApplication in the package se.app.av.bank.
* Run the main method.
## Execute the operation ##
* Consuming the exposed service in the endpoint:
```bat
curl --location --request GET 'http://localhost:8080/matchLetters?word1=army&word2=mary'
```