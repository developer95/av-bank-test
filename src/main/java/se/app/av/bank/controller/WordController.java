package se.app.av.bank.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import se.app.av.bank.service.OperationWords;

@RestController
@RequestMapping("/matchLetters")
public class WordController {
    private final OperationWords operationWords;

    @Autowired
    public WordController(OperationWords operationWords) {
        this.operationWords = operationWords;
    }

    /**
     * Match letters of the words
     * Consume example: http://${localhost}:${port}/matchLetters?word1=army&word2=mary
     *
     * @param word1 word.
     * @param word2 word.
     * @return match letters of the words status.
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public boolean matchLettersWords(@RequestParam String word1, @RequestParam String word2) {
        return operationWords.matchLetters(word1, word2);
    }
}
