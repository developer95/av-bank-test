package se.app.av.bank.service;

/**
 * Interface provider operations words
 */
public interface OperationWords {
    boolean matchLetters(String word1, String word2);
}
