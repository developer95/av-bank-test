package se.app.av.bank.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import se.app.av.bank.service.OperationWords;

import java.util.Arrays;

@Service
public class OperationWordsImpl implements OperationWords {
    private static final Logger LOGGER = LoggerFactory.getLogger(OperationWordsImpl.class);

    /**
     * Match letters of two string with the some length.
     *
     * @param word1 word.
     * @param word2 word.
     * @return boolean true if match the letters and length.
     */
    @Override
    public boolean matchLetters(String word1, String word2) {
        boolean match = false;
        if (word1 != null && word2 != null && (word1.length() == word2.length())) {
            LOGGER.info("words -> {},{}", word1, word2);
            final var lettersWord1 = Arrays.asList(word1.split(""));
            final var lettersWord2 = Arrays.asList(word2.split(""));
            match = lettersWord1
                    .stream()
                    .allMatch(letters -> lettersWord2.stream().anyMatch(letters::equals));

        }
        LOGGER.info("match words ? {}", match);
        return match;
    }
}
