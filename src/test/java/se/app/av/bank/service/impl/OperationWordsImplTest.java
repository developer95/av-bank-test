package se.app.av.bank.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import se.app.av.bank.service.OperationWords;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {OperationWordsImpl.class})
class OperationWordsImplTest {

    @Autowired
    private OperationWords operationWords;


    @Test
    void stageNullValues() {
        assertFalse(operationWords.matchLetters(null, null));
    }

    @Test
    void stage0() {
        assertTrue(operationWords.matchLetters("army", "mary"));
    }

    @Test
    void stage1() {
        assertFalse(operationWords.matchLetters("Army", "mary"));
    }

    @Test
    void stage2() {
        assertFalse(operationWords.matchLetters("Mary", "lary"));
    }

    @Test
    void stage3() {
        assertFalse(operationWords.matchLetters("test", "lary"));
    }

    @Test
    void stage4() {
        assertFalse(operationWords.matchLetters("testtest", "mary"));
    }
}